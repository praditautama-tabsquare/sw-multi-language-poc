import en from './en.json';
import id from './id.json';
import de from './de.json';

export {
    en,
    id,
    de
};