import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import './i18n.js';
// import './App.css';

function App() {
  const { t, i18n } = useTranslation();
  const [language, setLanguage] = useState("id");

  const handleChange = evt => {
    const lang = evt.target.value;
    console.log(lang);
    setLanguage(lang);
    i18n.changeLanguage(lang);
  };

  return (
    <div className="App">
      <div className="the-content">
        <header>
          <a href="/login" className="on-left back-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.42 90.55" className="svg-icon replaced-svg">
            <path d="M10.61,90.55,0,79.94,34.67,45.27,0,10.61,10.61,0l36,36a13.18,13.18,0,0,1,0,18.63Z">
            </path>
          </svg></a>
          <div href="#" className="logo-mid text-primary">
            <select onChange={handleChange  } value={language}>
              <option value="id">ID</option>
              <option value="en">EN</option>
              <option value="de">DE</option>
            </select>
          </div>
        </header>

        <div className="top-mask"></div>

        <ul className="order-sel">

          <li>
            <a href="/order-types/1" data-qa="ts-smw-button-dinein" data-text="Dine In">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 89.94 89.94" className="svg-icon replaced-svg">
                <path d="M45,0a45,45,0,1,0,45,45A45,45,0,0,0,45,0ZM31.45,49.63c-2.91,0-5.36-3.7-5.36-8.07s2.45-8.06,5.36-8.06,5.37,3.69,5.37,8.06S34.36,49.63,31.45,49.63ZM63,75V55.49a7,7,0,0,0,4.43-1.88c2.54-2.54,2.52-6.83,2.46-18.28,0-1.9,0-4,0-6.33a2.5,2.5,0,0,0-5,0c0,2.34,0,4.45,0,6.36,0,8.22.07,13.65-1,14.72a3.53,3.53,0,0,1-1.89.48V29a2.5,2.5,0,0,0-5,0V50.56a3.52,3.52,0,0,1-1.88-.48c-1.07-1.07-1-6.5-1-14.72,0-1.91,0-4,0-6.36a2.5,2.5,0,0,0-5,0c0,2.33,0,4.43,0,6.33C49,46.78,49,51.07,51.52,53.61A7,7,0,0,0,56,55.49V78.17a34.91,34.91,0,0,1-21,.3V56c5.12-1.84,8.87-7.61,8.87-14.44,0-8.3-5.55-15.06-12.37-15.06S19.09,33.26,19.09,41.56c0,6.83,3.75,12.6,8.86,14.44V75.51A35,35,0,1,1,63,75Z">
                </path>
              </svg>
              {t("order_type.dine_in")}
              <span></span>
            </a>
          </li>



          <li>
            <a href="/order-types/4" data-openpop="delivery" data-type="4" id="delivery-now" data-text="Delivery" data-later="true" data-qa="ts-smw-button-deliverynow">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86.87 86.96" className="svg-icon replaced-svg">
                <path d="M78.46,43.3c0-10.71-5.74-29.86-26.22-34.14,0-.11,0-.21,0-.31a8.85,8.85,0,0,0-17.7,0c0,.11,0,.21,0,.32C14.14,13.49,8.42,32.6,8.42,43.3v5h70ZM43.4,7a1.85,1.85,0,0,1,1.77,1.32c-.57,0-1.14,0-1.73,0s-1.21,0-1.8.05A1.84,1.84,0,0,1,43.4,7ZM19,38.3a31.11,31.11,0,0,1,2.75-8c4.09-7.94,11.38-12,21.66-12s17.57,4,21.66,12a31.49,31.49,0,0,1,2.75,8Z">
                </path>
                <path d="M83.37,61H3.5a3.5,3.5,0,0,0,0,7h7.91a13,13,0,1,0,23,0h17.9A13,13,0,1,0,75.4,68h8a3.5,3.5,0,1,0,0-7ZM28.93,74a6,6,0,1,1-6-6A6,6,0,0,1,28.93,74Zm40.94,0a6,6,0,1,1-6-6A6,6,0,0,1,69.87,74Z">
                </path>
                <path d="M6.53,58H80.41a3.5,3.5,0,0,0,0-7H6.53a3.5,3.5,0,0,0,0,7Z">
                </path>
              </svg>
              {t("order_type.delivery")}
              <span></span>
            </a>
            <input type="hidden" id="deliveryLater" value="1" />
          </li>


          <li>
            <a href="/order-types/6" data-qa="ts-smw-button-takeaway" data-type="6" data-text="Take Away" className="showpopup">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 89.94 89.94" className="svg-icon replaced-svg">
                <path d="M89.94,7.83A7.84,7.84,0,0,0,82.11,0H7.83A7.84,7.84,0,0,0,0,7.83V27.41L6.4,32.5.15,63.68A7.88,7.88,0,0,0,0,65.22V82.11a7.84,7.84,0,0,0,7.83,7.83H82.11a7.84,7.84,0,0,0,7.83-7.83V65.22a7.88,7.88,0,0,0-.15-1.54L83.56,32.56l6.38-5.18Zm-10,72.11H10V65.44l5.36-26.78,9.71-5.83,7.23,4.34a14.18,14.18,0,0,0,25.55,0l7.22-4.34,9.48,5.69,5.39,26.92ZM37.87,31a7.21,7.21,0,1,1,7.2,7.2A7.21,7.21,0,0,1,37.87,31Zm42.07-8.38-5.3,4.29-9.57-5.74-7,4.18a14.2,14.2,0,0,0-26.05,0l-7-4.18-9.6,5.76L10,22.59V10H79.94Z">
                </path>
              </svg>
              {t("order_type.take_away")}
              <span></span>
            </a>
          </li>

        </ul>
      </div>
    </div>
  );
}

export default App;
